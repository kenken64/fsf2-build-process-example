/*global module:false*/
module.exports = function(grunt) {


  // Load Grunt Plugins
  require('load-grunt-tasks')(grunt);


  //Configuration
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'), // read content from package.json

    //Banner
    banner:
    '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
    '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
    '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
    '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
    ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',



    // Validate Javascript Files
    jshint: {
      options: {
        reporterOutput: "",
        unused: true
      },
      development: ['public/assets/js/**/*.js']
    },

    // Validate CSS Files
    csslint: {
      development: {
       src: ['public/assets/css/**/*.css']
      }
    },

    // Validate HTML Files and Templates (HTML Fragments)
    htmlangular: {
      development: {
        src: [
          'public/views/**/*.template.html',
          'public/index.html'
        ]
      },

      options: {
        tmplext: 'template.html',
        reportpath: null,
        reportCheckstylePath: null
      }
    },

    // Merge my applications JS and CSS files
    concat: {
      options: {
        banner: '<%= banner %>'
      },
      css: {
        src: ['public/assets/css/*.css'],
        dest: 'publish/assets/css/main.css'
      },
      js: {
        src: ['public/assets/js/**/*.js'],
        dest: 'publish/assets/js/main.js'
      },
      bowerJS: {
        src: ['publish/assets/libs/**/*.js'],
        dest: 'publish/assets/js/libs.js'
      }
    },

    // Purge build files from the file system
    clean: {
      production: ['publish/']
    },


    // Minify JS
    uglify: {
      production: {
        options: {
          perserveComments: false,
          mangleProperties: true
        },
        files: {
          'publish/assets/js/main.min.js': ['publish/assets/js/main.js']
        }
      }
    },

    // Compile .less with LESS CSS Preprocessor
    less: {
      development: {
        files: [
          {'public/assets/css/main.css': 'public/assets/less/main.less'},
          {'public/assets/css/animations.css': 'public/assets/less/animations.less'}
        ]
      }
    },

    ngtemplates:  {
      app:        {
        cwd:  'public',
        src:  'views/**/*.template.html',
        dest: 'publish/views/templates.js'
      },
      htmlmin: {
        collapseBooleanAttributes:      true,
        collapseWhitespace:             true,
        removeAttributeQuotes:          true,
        removeComments:                 true,
        removeEmptyAttributes:          false,
        removeRedundantAttributes:      true,
        removeScriptTypeAttributes:     true,
        removeStyleLinkTypeAttributes:  true
      }
    },

    bower: {
      dev: {
        dest: 'publish/assets/',
        js_dest: 'publish/assets/libs/',
        css_dest: 'publish/assets/css/',
        fonts_dest: 'publish/assets/fonts/',
        images_dest: 'publish/assets/images/',
        options: {
          expand: false
        }
      }
    },

    watch: {
      less:{
        files: ['public/assets/less/**/*.less'],
        tasks: ['less']
      },
      scripts: {
        files: ['public/assets/js/**/*.js'],
        tasks: ['jshint']
      }
    },

    copy: {
      production: {
        files: [
          {
            expand: true,
            cwd: 'public/assets/',
            src: [
              'images/**',
              'videos/**',
              'fonts/**',
              'audio/**'
            ],
            dest: 'publish/assets/'
          }
        ]
      }
    },

    icons: {
      options: {
        platforms: ['ios', 'android']
      },

      ios: {
        options: {
          platforms: 'ios'
        },
        src: 'public/assets/mobileimages/*.jpg',
        dest: 'publish/assets/mobileimages/ios'
      },

      android: {
        options: {
          platforms: ['android'],
          expand: false
        },
        src: 'public/assets/mobileimages/*.jpg',
        dest: 'publish/assets/mobileimages/android'
      }
    }


  });




  // Default task.
  grunt.registerTask('default', ['jshint', 'csslint', 'htmlangular', 'clean', 'bower', 'concat']);
};
